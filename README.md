# CryptRaider

This project is a part of a tutorial series Unreal 5.0 C++ Developer: Learn C++ and Make Video Games from GameDev.tv which worked as an comprehensive and structured approach to learn about unreal further.

Topics covered:
- [x] lighting with lumen
- [x] modular level design
- [x] line tracing and collisions
- [x] simple physix interaction

Developed with Unreal Engine 5
